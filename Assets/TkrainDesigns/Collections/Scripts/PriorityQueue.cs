﻿using System.Collections.Generic;


namespace Tkraindesigns.Collections
{
    /// <summary>
    /// Implements a priority queue, similar to a queue, but allowing you to Enqueue to a position within the queue.  
    /// Uses a List to implement the queue.  
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PriorityQueue<T> where T : class
    {

        /// <summary>
        /// Used inside the PriorityQueue to store the element's priority and contents.
        /// </summary>
        protected class PQItem
        {
            /// <summary>
            /// used to sort the list based on priorities.
            /// </summary>
            public float priority;
            /// <summary>
            /// The element stored.  Note that this is a generic type T.
            /// </summary>
            public T contents;

            /// <summary>
            /// Initializer.
            /// </summary>
            /// <param name="newContent"></param>
            /// <param name="newPriority"></param>
            public PQItem(T newContent, float newPriority)
            {
                priority = newPriority;
                contents = newContent;
            }
        }

        /// <summary>
        /// The actual list.  Because we've declared PQItem.contents as type T, it is abstracted to the point that this class doesnt' care what it is until compile time.
        /// </summary>
        List<PQItem> queue = new List<PQItem>();

        /// <summary>
        /// Sometimes, you need to know the actual "priority" value.  If you call queue.NextPriority, this will return that priority.  Cache this before calling Dequeue()
        /// </summary>
        public float NextPriority
        {
            get
            {
                if (queue.Count > 0)
                {
                    return queue[0].priority;
                }
                else
                {
                    return 0;
                }
            }
        }
        /// <summary>
        /// The number of elements in the queue.
        /// </summary>
        public int Count
        {
            get
            {
                return queue.Count;
            }
        }

        /// <summary>
        /// Returns the next item in the queue, which by default has the lowest priority.
        /// </summary>
        /// <returns></returns>
        public T Dequeue()
        {
            if (queue.Count == 0)
                return null;
            PQItem result = queue[0];
            queue.Remove(result);
            return result.contents;
        }

        /// <summary>
        /// Returns the next item in the queue, and also places that item's priority in float priority.
        /// </summary>
        /// <param name="priority">The priority of the item returned.</param>
        /// <returns></returns>
        public T Dequeue(out float priority)
        {
            if (queue.Count == 0)
            {
                priority = 0;
                return null;
            }
            priority = queue[0].priority;
            return Dequeue();
        }

        /// <summary>
        /// Enqueues an item, using priority to fix its location in the queue.  
        /// </summary>
        /// <param name="item"></param>
        public void Enqueue(T item, float priority)
        {
            PQItem itemToAdd = new PQItem(item, priority);
            if (queue.Count == 0)
            {
                queue.Add(itemToAdd);
                return;
            }
            for (int i = 0; i < queue.Count; i++)
            {
                if (priority < queue[i].priority)
                {
                    queue.Insert(i, itemToAdd);
                    return;
                }
            }
            queue.Add(itemToAdd); //only reached when priority is highest.
        }

        /// <summary>
        /// Searches the queue for item, if found, returns true.  This is currently untested, and may need tweaking.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(T item)
        {
            if (queue.Count == 0)
            {
                return false;
            }
            foreach (PQItem test in queue)
            {
                if (test.contents.Equals(item))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// removes the item from the queue, if it exists.  Returns true if the item was in the queue, false if it was not.  This is currently untested, and may need tweaking.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(T item)
        {
            if (queue.Count == 0)
            {
                return false;
            }
            foreach (PQItem test in queue)
            {
                if (test.contents.Equals(item))
                {
                    queue.Remove(test);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Empties the queue.
        /// </summary>
        public void Clear()
        {
            queue.Clear();
        }

        /// <summary>
        /// Returns a list of the objects in the queue, in order of priority.
        /// </summary>
        /// <returns></returns>
        public List<T> ToList()
        {
            List<T> result = new List<T>();
            for (int i = 0; i < queue.Count; i++)
            {
                result.Add(queue[i].contents);
            }
            return result;
        }
        /// <summary>
        /// Returns an array of objects in the queue, in order of priority.
        /// </summary>
        /// <returns></returns>
        public T[] ToArray()
        {
            return ToList().ToArray();
        }
        /// <summary>
        /// Returns a Queue of the objects in the queue, in order of priority.
        /// </summary>
        /// <returns></returns>
        public Queue<T> ToQueue()
        {
            Queue<T> result = new Queue<T>();
            for (int i = 0; i <= queue.Count; i++)
            {
                result.Enqueue(queue[i].contents);
            }
            return result;
        }

        /// <summary>
        /// Returns a Stack of the elements in the queue, in order of priority.  I.e. POP will return the first item.
        /// </summary>
        /// <returns></returns>
        public Stack<T> toStack()
        {
            Stack<T> result = new Stack<T>();
            for (int i = queue.Count - 1; i >= 0; i--)
            {
                result.Push(queue[i].contents);
            }
            return result;
        }
    }

}