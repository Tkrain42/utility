﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tkraindesigns.Extentions
{

    public enum PositionWidgetType { Sphere, Cube, WireSphere, WireCube}
    public class PositionWidget : MonoBehaviour
    {
        [SerializeField]
        [Range(0.1f, 30)]
        float gizmoSize = 0.5f;
        [SerializeField]
        PositionWidgetType widgetType=PositionWidgetType.WireSphere;
        [SerializeField]
        Color gizmoColor = Color.yellow;
        [SerializeField]
        Tkraindesigns.Attributes.BaseMinMax baseMinMax;


        void OnDrawGizmos()
        {
            Gizmos.color = gizmoColor;
            switch (widgetType)
            {
                case PositionWidgetType.WireSphere:
                    {
                        Gizmos.DrawWireSphere(transform.position, gizmoSize);
                        break;
                    }
                case PositionWidgetType.Sphere:
                    {
                        Gizmos.DrawSphere(transform.position, gizmoSize);
                        break;
                    }
                case PositionWidgetType.WireCube:
                    {
                        Gizmos.DrawWireCube(transform.position, new Vector3(1, 1, 1) * gizmoSize);
                        break;
                    }
                case PositionWidgetType.Cube:
                    {
                        Gizmos.DrawCube(transform.position, new Vector3(1, 1, 1) * gizmoSize);
                        break;
                    }
            }
        }


    }
}
