﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Tkraindesigns.Attributes
{
    /// <summary>
    /// Class that provides a D&D style base damage + (dice per level) type functionality.  BaseValue is the absolute minimum value, what a "level 0" would have.  MinPerLevel and 
    /// MaxPerLevel is the range of value added for each level.   Calc(level) will generatate a number between Min and Max for each level, sum them, add to base and return them.
    /// </summary>
    [System.Serializable]
    public class BaseMinMax
    {
        [SerializeField]
        protected int baseValue = 10;
        [SerializeField]
        protected int minPerLevel = 2;
        [SerializeField]
        protected int maxPerLevel = 5;

        public int BaseValue {
            get { return baseValue; }
            set { baseValue = Mathf.Clamp(value, 0, int.MaxValue); }
        }

        public int MinPerLevel
        {
            get { return minPerLevel; }
            set
            {
                int newval = Mathf.Abs(value);
                if (newval > maxPerLevel) //Prevents setting of Min>Max
                {
                    minPerLevel = maxPerLevel;
                    maxPerLevel = newval;
                }
                else
                {
                    minPerLevel = newval;
                }
            }
        }

        public int MaxPerLevel
        {
            get { return maxPerLevel; }
            set
            {
                int newval = Mathf.Abs(value);
                if (newval < minPerLevel) //Prevents setting of Min>Max
                {
                    maxPerLevel = minPerLevel;
                    minPerLevel = newval;
                }
                else
                {
                    maxPerLevel = newval;
                }
            }
        }

        public int Calc(int Level)
        {
            int result = baseValue;
            for (int i = 1; i < Level; i++)
            {
                result += Random.Range(minPerLevel, maxPerLevel);
            }
            return result;
        }

        public int minRoll(int level)
        {
            return baseValue + (minPerLevel * level);
        }

        public int MaxRoll(int level)
        {
            return baseValue + (maxPerLevel * level);
        }
    }

}