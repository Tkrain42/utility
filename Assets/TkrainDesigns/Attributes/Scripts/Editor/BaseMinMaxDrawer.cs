﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace Tkraindesigns.Attributes { 
[CustomPropertyDrawer(typeof(BaseMinMax))]
public class BaseMinMaxDrawer : PropertyDrawer
{
    //Draw the property inside the given rect.
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
            var baseValue = property.FindPropertyRelative("baseValue");
            var minPerLevel = property.FindPropertyRelative("minPerLevel");
            var maxPerLevel = property.FindPropertyRelative("maxPerLevel");
            // Sanity checks
            if (baseValue.intValue < 0)
            {
                baseValue.intValue = 0;
            }
            if (minPerLevel.intValue < 0)
            {
                minPerLevel.intValue = 0;
            }
            if(maxPerLevel.intValue < 0)
            {
                maxPerLevel.intValue = 0;
            }
            if (minPerLevel.intValue > maxPerLevel.intValue)
            {
                minPerLevel.intValue = maxPerLevel.intValue;
            }
            if (maxPerLevel.intValue < minPerLevel.intValue)
            {
                maxPerLevel.intValue = minPerLevel.intValue;
            }
        
        EditorGUI.BeginProperty(position, label, property);
        string Tempstring = label.text;
        GUIContent newLabel = new GUIContent(Tempstring,"Base, Min/Max per level");
        //Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), newLabel);
        //Dont' indent child fields
        var indent = EditorGUI.indentLevel; //Save for later?
            var labelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 14f;
            GUIContent baseGUI = new GUIContent("B", "Base value");
            GUIContent minGUI = new GUIContent("v", "Min value per level.");
            GUIContent maxGUI = new GUIContent("^", "Max value per level.");

            position.width *= .33f;
            EditorGUI.PropertyField(position, baseValue, baseGUI);
            position.x +=position.width;
            EditorGUI.PropertyField(position, minPerLevel, minGUI);
            position.x += position.width;
            EditorGUI.PropertyField(position, maxPerLevel, maxGUI);

        EditorGUI.indentLevel = indent;
            EditorGUIUtility.labelWidth = labelWidth;
        EditorGUI.EndProperty();
    }
}
}
